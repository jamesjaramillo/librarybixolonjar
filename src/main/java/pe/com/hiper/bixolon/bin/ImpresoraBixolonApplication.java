package pe.com.hiper.bixolon.bin;

import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Stack;
import com.sun.jna.Native;
import pe.com.hiper.bixolon.service.impresoraBixolonAPI;
import pe.com.hiper.bixolon.common.ImpresoraConstants;

public class ImpresoraBixolonApplication {
	public static String LEFT = "1";
    public static String CENTER = "2";
    public static String RIGTH = "3";
	private final static int DATA_NOT_FOUND = 2;
	private final static int MAXIMUM_ATTEMPTS = 4;
	private final static int PRINTER_DISCONECT = -1;
	private static final Logger logger = LogManager.getLogger("ImpresoraBixolonApplication");
	public String pathLibraryDLL;

	public ImpresoraBixolonApplication(String pathLibraryDLL) {
		System.setProperty("jna.library.path", pathLibraryDLL);	
	}

	// public String getPathLibraryNetMasung() {
	// 	return pathLibraryDLL;
	// }

	// public void setPathLibraryNetMasung(String pathLibraryDLL) {
	// 	this.pathLibraryDLL = pathLibraryDLL;
	// }

	//method to get the first letter of every line on a text
	public String getFirstLetterOfEveryLine(String text) {
		String firstLetterOfEveryLine = "";
		String[] lines = text.split("\n");
		for (int i = 0; i < lines.length; i++) {
			firstLetterOfEveryLine += lines[i].substring(0, 1);
		}
		return firstLetterOfEveryLine;
	}

	public static void main(String[] args) throws Exception {
		String path = System.getProperty("user.dir");
		//System.out.println("Path: " + path);
		//System.setProperty("jna.library.path", path);	
		//impresoraBixolonAPI BXLAPI= (impresoraBixolonAPI)Native.load("BXLPAPI", impresoraBixolonAPI.class);
	
		//String text = "13ICPortalesLogo.png1322LOS PORTALES SA21RUC: 2030183789621Jr. Mariscal La Mar Nro. 99121Magdalena del Mar - Lima21Calle El Parque 150 - San Isidro21 FACTURA DE VENTA ELECTRONICA21005-0004000221Fecha: 19/12/2018  Hora: 16:3921Ticket: 01-00159221H. Ing.: 19/12/2018 16:33:5221H. Sal.: 19/12/2018 16:37:4021Tarifa: PACIENTE21Cajero: APS 321Doc. Identidad: RUC21Numero: 1040517909721HIPER S.A.1313CP";

		ImpresoraBixolonApplication app = new ImpresoraBixolonApplication(path);
		String text = "13ICPortalesLogo.png13BC987643210981322Los Portales21             SEDE PRINCIPAL             21     Av. de las Américas #57, Bogotá     2322NOTA DE CREDITO 1321TERM:   20170917       REF  : 00577721FECHA:  26ENE2022      HORA  :  18:431311___________________________________________1323PRODUCTO        :   EFECTIVO23NRO DE TICKET   :     00159211___________________________________________1323MONTO INGRESADO    :    S/   52.0023MONTO DEVUELTO     :    S/    0.0023MONTO POR DEVOLVER :    S/   52.001321POR FAVOR ACERCATE A UN ANFITRION PARA21SOLUCIONAR EL INCONVENIENTE1313CP";

		//String text = "1322LOS PORTALES SA21RUC: 2030183789621Jr. Mariscal La Mar Nro. 99121Magdalena del Mar - Lima21Calle El Parque 150 - San Isidro21 FACTURA DE VENTA ELECTRONICA21005-0004000221Fecha: 19/12/2018  Hora: 16:3921Ticket: 01-00159221H. Ing.: 19/12/2018 16:33:5221H. Sal.: 19/12/2018 16:37:4021Tarifa: PACIENTE21Cajero: APS 321Doc. Identidad: RUC21Numero: 1040517909721HIPER S.A.1313CP"
		String imgPath = "D:\\imagen\\";
		app.printVoucher("123456", text,imgPath,0,0,0,2,0);	
		
		
		// impresoraBixolonAPI BXLAPI= (impresoraBixolonAPI)Native.load("BXLPAPI", impresoraBixolonAPI.class);

		// // System.out.println("BXLAPI.PrinterClose(): " + BXLAPI.PrinterClose());
		// // try {
		// // 	Thread.sleep(2000);
		// // } catch (InterruptedException e) {
		// // 	e.printStackTrace();
		// // }

		// if(BXLAPI.ConnectUsb() == 0){
		// 	System.out.println("Connected");
		// 	BXLAPI.TransactionStart();
		// 	BXLAPI.InitializePrinter();
		// 	BXLAPI.SetCharacterSet(ImpresoraConstants.BXL_CS_WPC1252);
        //     BXLAPI.SetInterChrSet(ImpresoraConstants.BXL_ICS_USA);
			
		// 	BXLAPI.PrintText("FELIZ AÑO NUEVO!", ImpresoraConstants.BXL_ALIGNMENT_CENTER, ImpresoraConstants.BXL_FT_UNDERLINE, ImpresoraConstants.BXL_TS_0WIDTH | ImpresoraConstants.BXL_TS_0HEIGHT);
		// 	BXLAPI.PrintText("\n", ImpresoraConstants.BXL_ALIGNMENT_CENTER, ImpresoraConstants.BXL_FT_UNDERLINE, ImpresoraConstants.BXL_TS_0WIDTH | ImpresoraConstants.BXL_TS_0HEIGHT);
		// 	BXLAPI.PrintText("FELIZ AÑO NUEVO!", ImpresoraConstants.BXL_ALIGNMENT_CENTER, ImpresoraConstants.BXL_FT_UNDERLINE, ImpresoraConstants.BXL_TS_0WIDTH | ImpresoraConstants.BXL_TS_0HEIGHT);
		// 	BXLAPI.PrintText("\n", ImpresoraConstants.BXL_ALIGNMENT_CENTER, ImpresoraConstants.BXL_FT_UNDERLINE, ImpresoraConstants.BXL_TS_0WIDTH | ImpresoraConstants.BXL_TS_0HEIGHT);
		// 	BXLAPI.PrintText("FELIZ AÑO NUEVO!", ImpresoraConstants.BXL_ALIGNMENT_CENTER, ImpresoraConstants.BXL_FT_UNDERLINE, ImpresoraConstants.BXL_TS_0WIDTH | ImpresoraConstants.BXL_TS_0HEIGHT);
		// 	BXLAPI.PrintText("\n", ImpresoraConstants.BXL_ALIGNMENT_CENTER, ImpresoraConstants.BXL_FT_UNDERLINE, ImpresoraConstants.BXL_TS_0WIDTH | ImpresoraConstants.BXL_TS_0HEIGHT);
			
		// 	BXLAPI.CutPaper();

		// 	BXLAPI.PaperEject(ImpresoraConstants.BXL_EJT_HOLD);

		// 	if (BXLAPI.TransactionEnd(true, 5000 /* 3 seconds */) != ImpresoraConstants.BXL_SUCCESS){
        //         // failed to read a response from the printer after sending the print-data.
		// 		System.out.println("TransactionEnd failed");
        //     }

		// 	//int status = printerStatus("123456");
		// 	//System.out.println("Status: " + status);
		//  }else{
		// 	System.out.println("No se pudo conectar a la impresora");
		// }	
	}

	public synchronized int printVoucher(String trace, String voucher, String pathImage, int densityImage,
	int HRIcharactersBarcode, int typeBarcode, int widthCharacterBarcode, int symbolSizeQR) throws Exception {
		System.out.println("**-Trace: " + trace + " **-Trama: " + voucher + " *-Imagen: " + pathImage + " **-Densidad: "
		+ densityImage + " **-HRIcharactersBarcode: " + HRIcharactersBarcode + " **-typeBarcode: " + typeBarcode
		+ " **-withhCharacterBarcode: " + widthCharacterBarcode + " **-symbolSizeQR: " + symbolSizeQR);
		int resultado = -1;
		int conexion = printerStatus(trace);
		System.out.println("Printer Status: " + conexion);
		if (conexion == 0 || conexion == 1 || conexion == 7) {
			if (!"".equals(voucher)) {
				// 1 = SUCCESS
				resultado = imprimir(voucher,pathImage,densityImage,HRIcharactersBarcode,typeBarcode,widthCharacterBarcode, symbolSizeQR); 
				System.out.println("printVoucher/resutado: " + resultado);

			} else {
				resultado = DATA_NOT_FOUND;
				System.out.println("printVoucher/resutado: " + resultado);
			}
		} else {
			resultado = conexion;
			System.out.println("printVoucher/resutado: " + resultado);
		}
		System.out.println("printVoucher/resutado: " + resultado);
		return resultado;
		}
	
	public int imprimir(String voucher, String pathImage, int densityImage,
	int HRIcharactersBarcode, int typeBarcode, int widthCharacterBarcode, int symbolSizeQR){
		impresoraBixolonAPI BXLAPI= (impresoraBixolonAPI)Native.load("BXLPAPI", impresoraBixolonAPI.class);
		int result = -1;
		int attemps = 0;
		String strConfig = "";
		String textoLinea = "";
		int typeFont = 0;
		int fontWidth = 0;
		int fontHeight = 0;
	
		int HRITextPosition = ImpresoraConstants.BXL_BC_TEXT_NONE;
		int typeBar = ImpresoraConstants.BXL_BCS_UPCA;
		
		if(densityImage == 1){
			densityImage = 100;
		}else{
			densityImage = 50;
		}

		BXLAPI.TransactionStart();
		BXLAPI.InitializePrinter();
		BXLAPI.SetCharacterSet(ImpresoraConstants.BXL_CS_WPC1252);
        BXLAPI.SetInterChrSet(ImpresoraConstants.BXL_ICS_USA);

		//do {
			try{		
				String[] lines = voucher.split("\u001C");
				for (int i = 0; i < lines.length; i++) {
					strConfig = lines[i].substring(0, 2);
					textoLinea = lines[i].substring(2);
					//System.out.println(strConfig + " textoLinea: " + textoLinea);
					String firstDigit = strConfig.substring(0,1);
					String secondDigit = strConfig.substring(1,2);

					//SEND IMAGE
					if (firstDigit.equals("I")){
						String pathImg = pathImage + textoLinea;
					 	String filePathExt = pathImg.substring(pathImg.lastIndexOf("."));
					 if (filePathExt.equalsIgnoreCase(".BMP") || filePathExt.equalsIgnoreCase(".JPG")
							 || filePathExt.equalsIgnoreCase(".PNG")) {
						 File img=new File(pathImg);
						 long imgSize = img.length();
						 if(imgSize<=10240 && imgSize != 0){
							System.out.println("Imagen ** Size: " + imgSize);
							if (secondDigit.equals("L")){
							BXLAPI.PrintBitmap(pathImg, ImpresoraConstants.BXL_WIDTH_NONE, ImpresoraConstants.BXL_ALIGNMENT_LEFT, densityImage, false);
							}else if (secondDigit.equals("R")){
							BXLAPI.PrintBitmap(pathImg, ImpresoraConstants.BXL_WIDTH_NONE, ImpresoraConstants.BXL_ALIGNMENT_RIGHT, densityImage, false);
							}else if (secondDigit.equals("C")){
							BXLAPI.PrintBitmap(pathImg, ImpresoraConstants.BXL_WIDTH_NONE, ImpresoraConstants.BXL_ALIGNMENT_CENTER, densityImage, false);
							}
						 }else{
							 System.out.println("PICTURE EXCEED (10240) SUPPORTED SIZE {"+pathImg+"} OR EMPTY ** SIZE: {"+imgSize+"}");
						 }
					 }else{
						 System.out.println("You can only print images in format BMP");
					 }                                
					}else if (firstDigit.equals("B")){
						if(typeBarcode == 0){
							typeBar = ImpresoraConstants.BXL_BCS_UPCA;
							widthCharacterBarcode = widthCharacterBarcode +2;
						}else if(typeBarcode == 1){
							typeBar = ImpresoraConstants.BXL_BCS_UPCE;
							widthCharacterBarcode = widthCharacterBarcode +2;
						}else if(typeBarcode == 2){
							typeBar = ImpresoraConstants.BXL_BCS_JAN13;
							widthCharacterBarcode = widthCharacterBarcode +2;
						}else if(typeBarcode == 3){	
							typeBar = ImpresoraConstants.BXL_BCS_JAN8;
							widthCharacterBarcode = widthCharacterBarcode +2;
						}else if(typeBarcode == 4){
							widthCharacterBarcode = widthCharacterBarcode +1;
							typeBar = ImpresoraConstants.BXL_BCS_CODE39;
						}else if(typeBarcode == 5){
							widthCharacterBarcode = widthCharacterBarcode +1;
							typeBar = ImpresoraConstants.BXL_BCS_ITF;
						}else if(typeBarcode == 6){
							widthCharacterBarcode = widthCharacterBarcode +1;
							typeBar = ImpresoraConstants.BXL_BCS_CODABAR;
						}else if(typeBarcode == 7){
							widthCharacterBarcode = widthCharacterBarcode +1;
							typeBar = ImpresoraConstants.BXL_BCS_CODE128;
						}
						if(HRIcharactersBarcode == 0){
							HRITextPosition = ImpresoraConstants.BXL_BC_TEXT_NONE;
						} else if(HRIcharactersBarcode == 1){
							HRITextPosition = ImpresoraConstants.BXL_BC_TEXT_ABOVE;
						} else if(HRIcharactersBarcode == 2){
							HRITextPosition = ImpresoraConstants.BXL_BC_TEXT_BELOW;
						} else if(HRIcharactersBarcode == 3){
							HRITextPosition = ImpresoraConstants.BXL_BC_TEXT_BOTH;
						}

						switch (secondDigit) {
							case "L":
								BXLAPI.PrintBarcode(textoLinea, typeBar, 160, widthCharacterBarcode, ImpresoraConstants.BXL_ALIGNMENT_LEFT, HRITextPosition);
								break;
							case "R":
								BXLAPI.PrintBarcode(textoLinea, typeBar, 160, widthCharacterBarcode, ImpresoraConstants.BXL_ALIGNMENT_RIGHT, HRITextPosition);
								break;
							case "C":
								BXLAPI.PrintBarcode(textoLinea, typeBar, 160, widthCharacterBarcode, ImpresoraConstants.BXL_ALIGNMENT_CENTER, HRITextPosition);
								break;
						}	

					}
					else{
					switch (secondDigit) {
						case "0":
							//System.out.println("fontType - “0”: tamaño de fuente 10px");
							typeFont = ImpresoraConstants.BXL_FT_DEFAULT;
							fontWidth = ImpresoraConstants.BXL_TS_0WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_0HEIGHT;
							break;

						case "1":
							//System.out.println("fontType - '1': tamaño de fuente negrita 10px");
							typeFont = ImpresoraConstants.BXL_FT_BOLD;
							fontWidth = ImpresoraConstants.BXL_TS_0WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_0HEIGHT;
							break;
						
						case "2":
							//System.out.println("fontType - '2': tamaño de fuente 10px doble alto");
							typeFont = ImpresoraConstants.BXL_FT_DEFAULT;
							fontWidth = ImpresoraConstants.BXL_TS_0WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_1HEIGHT;
							break;

						case "3":
							//System.out.println("fontType - '3': tamaño de fuente 10px negrita doble alto");
							typeFont = ImpresoraConstants.BXL_FT_BOLD;
							fontWidth = ImpresoraConstants.BXL_TS_0WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_1HEIGHT;
							break;

						case "4":
							//System.out.println("fontType - '4': tamaño de fuente 10px doble ancho");
							typeFont = ImpresoraConstants.BXL_FT_DEFAULT;
							fontWidth = ImpresoraConstants.BXL_TS_1WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_0HEIGHT;
							break;

						case "5":
							//System.out.println("fontType - '5': tamaño de fuente 10px negrita doble ancho");
							typeFont = ImpresoraConstants.BXL_FT_BOLD;
							fontWidth = ImpresoraConstants.BXL_TS_1WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_0HEIGHT;
							break;

						case "6":
							System.out.println("fontType - '6': tamaño de fuente 14px");
							typeFont = ImpresoraConstants.BXL_FT_DEFAULT;
							fontWidth = ImpresoraConstants.BXL_TS_2WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_2HEIGHT;
							break;
							
						case "7":
							//System.out.println("fontType - '7': tamaño de fuente 14px negrita");
							typeFont = ImpresoraConstants.BXL_FT_BOLD;
							fontWidth = ImpresoraConstants.BXL_TS_2WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_2HEIGHT;
							break;
						
						case "8":
							//System.out.println("fontType - '8': tamaño de fuente 26px negrita");
							typeFont = ImpresoraConstants.BXL_FT_BOLD;
							fontWidth = ImpresoraConstants.BXL_TS_4WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_4HEIGHT;
							break;
						
						case "9":
							//System.out.println("fontType - '9': tamaño de fuente 28px negrita doble ancho");
							typeFont = ImpresoraConstants.BXL_FT_BOLD;
							fontWidth = ImpresoraConstants.BXL_TS_6WIDTH;
							fontHeight = ImpresoraConstants.BXL_TS_5HEIGHT;
							break;
						}
						
						switch (firstDigit) {
							case "1":
								BXLAPI.PrintText(textoLinea + "\n", ImpresoraConstants.BXL_ALIGNMENT_LEFT, typeFont, fontWidth | fontHeight);
								break;
							case "2":
								BXLAPI.PrintText(textoLinea + "\n", ImpresoraConstants.BXL_ALIGNMENT_CENTER, typeFont, fontWidth | fontHeight);
								break;
							case "3":
								BXLAPI.PrintText(textoLinea + "\n", ImpresoraConstants.BXL_ALIGNMENT_RIGHT, typeFont, fontWidth | fontHeight);
								break;
						}	
					}
			}
			
			if(strConfig.equals("CP")) {
				BXLAPI.CutPaper();
				BXLAPI.PaperEject(ImpresoraConstants.BXL_EJT_HOLD);

				if (BXLAPI.TransactionEnd(true, 5000 /* 3 seconds */) != ImpresoraConstants.BXL_SUCCESS){
					// failed to read a response from the printer after sending the print-data.
					result = -1;
					System.out.println("TransactionEnd failed");
				}
			}

			result = 0;

			// if(strConfig.equals("CP")) {
			// 	break;
			// }

		} catch (Exception e) {
			result = -1;
			attemps++;
			e.printStackTrace();
		}

			// if (attemps == MAXIMUM_ATTEMPTS) {
			// 	System.out.println("IMPRESORA DESCONECTADA");
			// 	result = PRINTER_DISCONECT;
			// 	break;
			// }
		//} while (result == 0);


		return result;
	}

	
	public synchronized int printerStatus(String trace) throws Exception {
		try{
			return testearImpresora();
		}catch(Exception e){
			System.out.println("Error en printerStatus: " + e.getMessage());
			return -1;
		}
	}

	public int testearImpresora() {
		impresoraBixolonAPI BXLAPI= (impresoraBixolonAPI)Native.load("BXLPAPI", impresoraBixolonAPI.class);
		BXLAPI.ConnectUsb();
		int ret = 99;
		int printerStatus = BXLAPI.GetPrinterCurrentStatus();
		int presenterStatus = BXLAPI.GetPresenterStatus();
		System.out.println("printerStatus: " + printerStatus + " ** status: " + presenterStatus);
		
		
		switch(printerStatus) {
			case ImpresoraConstants.BXL_STS_NORMAL:
				ret =  0;
				break;
			case ImpresoraConstants.BXL_STS_PAPEREMPTY:
				ret =  2;
				break;
			case ImpresoraConstants.BXL_STS_COVEROPEN:
				ret =  3;
				break;
			case ImpresoraConstants.BXL_STS_PAPER_NEAR_END:
				ret =  1;
				break;
			case ImpresoraConstants.BXL_STS_PAPER_TO_BE_TAKEN:
				ret =  7;
				break;
			case ImpresoraConstants.BXL_STS_ERROR:
				ret =  -1;
				break;
		}
		
		switch(presenterStatus) {
			case ImpresoraConstants.BXL_PRT_STS_NORMAL:
				ret = 0;
				break;
			case ImpresoraConstants.BXL_PRT_STS_PAPER_NEAR_END:
				ret = 1;
				break;
			case ImpresoraConstants.BXL_PRT_STS_PAPEREMPTY:
				ret = 2;
				break;
			case ImpresoraConstants.BXL_PRT_STS_PAPER_JAM:
				ret = 4;
				break;
			case ImpresoraConstants.BXL_PRT_STS_PAPER_IN:
				ret = 7;
				break;
		}
		

		return ret;
	}
}
