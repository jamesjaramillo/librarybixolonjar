package pe.com.hiper.bixolon.service;

import com.sun.jna.Library;

public interface impresoraBixolonAPI extends Library {
    public int ConnectUsb();
   
    public int PrinterClose();

    
    /* Enters or Leaves 'Page' mode, and then sends the data in the buffer to the printer*/
    public int SetPagemode(int pageMode);

    /*** PrintText Methods. If the method succeeds, the return value is 0. Other cases, returns negative values ***/
        
        /* Enters 'Transaction' mode. */
        public int TransactionStart();

        /*Reset all printer settings to the state they were in when the printer was powered on*/
        public int InitializePrinter();

        /* Sets the code table used for printing characters
            BXL_CS_WPC1252 = 16 -> Latin 1 */
        public int SetCharacterSet(int codeChrSet);

        /* Sets the character set used for printing some of international characters 
            BXL_ICS_LATIN = 12 ->  LATIN AMERICA code setting */
        public int SetInterChrSet(int codeInternationalChr);

        /*Sends a character print command to the printer 
         @param Alignment: BXL_ALIGNMENT_LEFT, BXL_ALIGNMENT_CENTER, BXL_ALIGNMENT_RIGHT
         @param Attribute: BXL_FT_DEFAULT, BXL_FT_FONTB, BXL_FT_BOLD, BXL_FT_UNDERLINE, BXL_FT_UNDERTHICK, BXL_FT_REVERSE, BXL_FT_FONTC, BXL_FT_RED_COLOR
         @param TextSize: BXL_TS_0WIDTH ... BXL_TS_7WIDTH & BXL_TS_0HEIGHT ... BXL_TS_7HEIGHT
        */
        public int PrintText(String Data, int Alignment, int Attribute, int TextSize);

        /*Sends line-feeds commands to the printer
          @param nLines: Range: 1 ≤ Feed ≤ 255
        */
        public int LineFeed(int nLines);

        /* Sends a paper cut commands with line feeds to the printer */
        public int CutPaper();

        //Only available to presenter that is attached at BK3-3 KIOSK printer:
        /* BXL_EJT_NONE = 0 -> Ejects paper all
           BXL_EJT_HOLD = 1 -> Ejects paper with hold(Default) */
        public int PaperEject(int option);

        /*Leaves 'Transaction' mode, and then sends print data in the buffer to start printing */
        public int TransactionEnd(boolean SendCompletedCheck, int Timeout);

    /*** Get Status ***/
        /*  BXL_STS_NORMAL = 0 -> Normal. (No errors)
            BXL_STS_PAPEREMPTY = 1 -> Paper Empty
            BXL_STS_COVEROPEN = 2 -> Printer cover is open
            BXL_STS_PAPER_NEAR_END = 4 -> Paper is near-end
            BXL_STS_CASHDRAWER_HIGH = 16 -> Cash drawer Pin is in high state.
            BXL_STS_ERROR = 32 -> Offline (Printer is disconnected with HOST)
            BXL_STS_NOT_OPEN = 64 -> Not ready to start communication with the printer.
            BXL_STS_BATTERY_LOW = 128 -> Battery runs low
            BXL_STS_PAPER_TO_BE_TAKEN = 256 -> Printed receipt(label) presence is detected.
            BXL_STS_CASHDRAWER_LOW = 512 -> Cash drawer Pin is in low state. 
        */
        public int GetPrinterCurrentStatus();

        /*
        BXL_PRT_STS_NORMAL 0 -> Normal. (No errors)
        BXL_PRT_STS_PAPER_NEAR_END 1 -> Paper is near-end
        BXL_PRT_STS_PAPEREMPTY 4 -> Paper empty
        BXL_PRT_STS_PAPER_IN 8 -> Paper is detected at presenter
        BXL_PRT_STS_PAPER_JAM 128 -> Paper jam
        */
        public int GetPresenterStatus();

        /*
        Width: 
            BXL_WIDTH_FULL -> -1 Maximum width the printer can print
            BXL_WIDTH_NONE -> -2 No variation given to image size
        Alignment:
            BXL_ALIGNMENT_LEFT -> 0 Left-aligned
            BXL_ALIGNMENT_CENTER -> 1 Centered
            BXL_ALIGNMENT_RIGHT -> 2 Right-aligned
        Level:
            The brightness level. (Range: 0 ≤ Level ≤ 100)
        Dithering:
            Whether apply the dither option or not.
        */
        public int PrintBitmap(String FileName, int Width, int Alignment, int Level, boolean bDithering);

        public int PrintBarcode(String Data, int Symbology, int Height, int Width, int Alignment, int TextPosition);






     //public int GetStat();
}
