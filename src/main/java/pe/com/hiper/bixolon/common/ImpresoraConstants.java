package pe.com.hiper.bixolon.common;

public class ImpresoraConstants {

    //CharaterSet
    public static final int BXL_CS_PC437			    = 0;	// (USA, Standard Europe)
    public static final int BXL_CS_KATAKANA		        = 1;	// Katakana
    public static final int BXL_CS_PC850			    = 2;	// 850 (Multilingual)
    public static final int BXL_CS_PC860			    = 3;	// 860 (Portuguese)
    public static final int BXL_CS_PC863			    = 4;	// 863 (Canadian-French)
    public static final int BXL_CS_PC865			    = 5;	// 865 (Nordic)
    public static final int BXL_CS_WPC1252		        = 16;   // 1252 (Latin I)
    public static final int BXL_CS_PC866			    = 17;   // 866 (Cyrillic #2)
    public static final int BXL_CS_PC852			    = 18;	// 852 (Latin 2)
    public static final int BXL_CS_PC858			    = 19;	// 858 (Euro)
    public static final int BXL_CS_PC862			    = 21;	// 862 (Hebrew DOS code)
    public static final int BXL_CS_PC864			    = 22;	// NOT Supported / 864 (Arabic)
    public static final int BXL_CS_THAI42			    = 23;	// THAI42
    public static final int BXL_CS_WPC1253		        = 24;	// 1253 (Greek)
    public static final int BXL_CS_WPC1254		        = 25;	// 1254 (Turkish)
    public static final int BXL_CS_WPC1257		        = 26;	// 1257 (Baltic)
    public static final int BXL_CS_FARSI			    = 27;	// NOT Supported / FARSI
    public static final int BXL_CS_WPC1251		        = 28;	// 1251 (Cyrillic)
    public static final int BXL_CS_PC737			    = 29;	// 737 (Greek)
    public static final int BXL_CS_PC775			    = 30;	// 775 (Baltic)
    public static final int BXL_CS_THAI14			    = 31;	// THAI14
    public static final int BXL_CS_WPC1255		        = 33;	// 1255 (Hebrew New Code)
    public static final int BXL_CS_THAI11			    = 34;	// THAI11
    public static final int BXL_CS_THAI18			    = 35;	// THAI18
    public static final int BXL_CS_PC855			    = 36;	// 855 (Cyrillic)
    public static final int BXL_CS_PC857			    = 37;	// 857 (Turkish)
    public static final int BXL_CS_PC928			    = 38;	// 928 (Greek)
    public static final int BXL_CS_THAI16			    = 39;	// THAI16
    public static final int BXL_CS_WPC1256		        = 40;	// NOT Supported / 1256 (Arabic)
    public static final int BXL_CS_PC1258			    = 41;	// 1258 (Vietnam)
    public static final int BXL_CS_KHMER			    = 42;	// NOT Supported / KHMER(Cambodia)
    public static final int BXL_CS_PC1250			    = 47;	// 1250 (Czech)
    public static final int BXL_CS_LATIN9			    = 48;	// Latin 9
    public static final int BXL_CS_TCVN			        = 49;	// TCVN-3 (Vietnamese)
    public static final int BXL_CS_TCVN_CAPITAL	        = 50;	// TCVN-3 (Vietnamese)
    public static final int BXL_CS_VISCII               = 51;	// VISCII (Vietnamese)
    public static final int BXL_CS_CP912                = 52;	// 912 (Albania)
    public static final int BXL_CS_USER			        = 255;  // User Code Page
    public static final int BXL_CS_UTF8                 = 65001;// UTF-8
    public static final int BXL_CS_KS5601               = 949;	// KOREAN
    public static final int BXL_CS_BIG5			        = 950;	// CHINESE (BIG5)
    public static final int BXL_CS_GB2312			    = 936;	// Simplified CHINESE (GB2312)
    public static final int BXL_CS_SHIFT_JIS		    = 932;	// JAPAN (Shift JIS)

    //International CharacterSet
    public static final int BXL_ICS_USA			        = 0;
    public static final int BXL_ICS_FRANCE		        = 1;
    public static final int BXL_ICS_GERMANY		        = 2;
    public static final int BXL_ICS_UK				    = 3;
    public static final int BXL_ICS_DENMARK1	        = 4;
    public static final int BXL_ICS_SWEDEN		        = 5;
    public static final int BXL_ICS_ITALY			    = 6;
    public static final int BXL_ICS_SPAIN			    = 7;
    public static final int BXL_ICS_JAPAN			    = 8;
    public static final int BXL_ICS_NORWAY		        = 9;
    public static final int BXL_ICS_DENMARK2	        = 10;
    public static final int BXL_ICS_SPAIN2			    = 11;
    public static final int BXL_ICS_LATIN			    = 12;   //LATIN AMERICA code setting	
    public static final int BXL_ICS_KOREA			    = 13;
    public static final int BXL_ICS_SLOVENIA            = 14;
    public static final int BXL_ICS_CHINA               = 15;
   
    //Alignment
    public static final int BXL_ALIGNMENT_LEFT          = 0;
    public static final int BXL_ALIGNMENT_CENTER        = 1;
    public static final int BXL_ALIGNMENT_RIGHT         = 2;

    //Text Attribute
    public static final int BXL_FT_DEFAULT              = 0;
    public static final int BXL_FT_FONTB			    = 1; 
    public static final int BXL_FT_BOLD			        = 2;
    public static final int BXL_FT_UNDERLINE		    = 4;
    public static final int BXL_FT_UNDERTHICK	        = 6;
    public static final int BXL_FT_REVERSE			    = 8;
    public static final int BXL_FT_UPSIDEDOWN           = 10;
    public static final int BXL_FT_FONTC                = 16;
    public static final int BXL_FT_RED_COLOR            = 64;

     //Text Size
    public static final int BXL_TS_0WIDTH		        = 0;
    public static final int BXL_TS_1WIDTH		        = 16;
    public static final int BXL_TS_2WIDTH		        = 32;
    public static final int BXL_TS_3WIDTH		        = 48;
    public static final int BXL_TS_4WIDTH		        = 64;
    public static final int BXL_TS_5WIDTH		        = 80;
    public static final int BXL_TS_6WIDTH		        = 96;
    public static final int BXL_TS_7WIDTH		        = 112;

    public static final int BXL_TS_0HEIGHT	            = 0;
    public static final int BXL_TS_1HEIGHT	            = 1;
    public static final int BXL_TS_2HEIGHT	            = 2;
    public static final int BXL_TS_3HEIGHT	            = 3;
    public static final int BXL_TS_4HEIGHT	            = 4;
    public static final int BXL_TS_5HEIGHT	            = 5;
    public static final int BXL_TS_6HEIGHT	            = 6;
    public static final int BXL_TS_7HEIGHT	            = 7;   

    // Eject Options
    public static final int BXL_EJT_NONE		        = 0;
    public static final int BXL_EJT_HOLD                = 1;

    // ERROR RESULTS
    public static final int BXL_SUCCESS				    = 0;
    public static final int BXL_READBUFFER_EMPTY        = -1;
    public static final int BXL_CONNECT_FAIL		    = -100;
    public static final int BXL_NOT_OPENED			    = -101;
    public static final int BXL_NOT_SUPPORT		        = -107;
    public static final int BXL_INVALID_PARAM		    = -108;
    public static final int BXL_BUFFER_ERROR		    = -109;
    public static final int BXL_NO_PRINT_DATA		    = -110;
    public static final int BXL_COMPLETE_ERROR		    = -111;
    public static final int BXL_NO_TRANSACTION_START    = -112;
    public static final int BXL_TEXT_ENCODING_ERROR	    = -120;
    public static final int BXL_PAGE_MODE_ALREADY_IN    = -130;
    public static final int BXL_NOT_IN_PAGE_MODE	    = -131;
    public static final int BXL_REGISTRY_ERROR		    = -200;
    public static final int BXL_WRITE_ERROR			    = -300;
    public static final int BXL_READ_ERROR			    = -301;
    public static final int BXL_BMP_LOAD_ERROR		    = -400;
    public static final int BXL_BMP_BUFFER_SIZE_ERROR	= -401;
    public static final int BXL_BC_DATA_ERROR			= -500;
    public static final int BXL_BC_NOT_SUPPORT          = -501;

    public static final int BXL_STS_NORMAL		        = 0;
    public static final int BXL_STS_PAPEREMPTY	        = 1;
    public static final int BXL_STS_COVEROPEN	        = 2;
    public static final int BXL_STS_PAPER_NEAR_END      = 4;
    public static final int BXL_STS_AUTOCUTTER_ERROR    = 8;
    public static final int BXL_STS_CASHDRAWER_HIGH	    = 16;
    public static final int BXL_STS_ERROR		        = 32;
    public static final int BXL_STS_NOT_OPEN	        = 64;
    public static final int BXL_STS_BATTERY_LOW         = 128;
    public static final int BXL_STS_PAPER_TO_BE_TAKEN   = 256;
    public static final int BXL_STS_CASHDRAWER_LOW      = 512;

    //Presenter Status flag
    public static final int BXL_PRT_STS_NORMAL          = 0;
    public static final int BXL_PRT_STS_PAPER_NEAR_END  = 1;
    public static final int BXL_PRT_STS_PAPEREMPTY      = 4;
    public static final int BXL_PRT_STS_PAPER_IN        = 8;
    public static final int BXL_PRT_STS_PAPER_JAM       = 128;

    // IMAGE WIDTH
    public static final int BXL_WIDTH_FULL              = -1;
    public static final int BXL_WIDTH_NONE              = -2;

     // BARCODE
     public static final int BXL_BCS_PDF417			                                = 200;
     public static final int BXL_BCS_QRCODE_MODEL2 	                                = 202;
     public static final int BXL_BCS_QRCODE_MODEL1 	                                = 203; 
     public static final int BXL_BCS_DATAMATRIX		                                = 204;
     public static final int BXL_BCS_MAXICODE_MODE2	                                = 205;
     public static final int BXL_BCS_MAXICODE_MODE3	                                = 206;
     public static final int BXL_BCS_MAXICODE_MODE4	                                = 207;
     public static final int BXL_BCS_2D_GS1DATABAR_STACKED                          = 208;
     public static final int BXL_BCS_2D_GS1DATABAR_STACKED_OMNIDIRECTIONAL          = 209;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_EAN8                         = 210;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_EAN13                        = 211;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_UPCA                         = 212;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_UPCE                         = 213;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_GS1DATABAR_OMNIDIRECTIONAL           = 214;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_GS1DATABAR_TRUNCATED                 = 215;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_GS1DATABAR_STACKED                   = 216;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_GS1DATABAR_STACKED_OMNIDIRECTIONAL   = 217;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_GS1DATABAR_LIMITED                   = 218;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_GS1DATABAR_EXPANDED                  = 219;
     public static final int BXL_BCS_COMPOSITE_SYMBOLE_GS1_128                              = 220;

     public static final int BXL_BCS_UPCA			                = 101;
     public static final int BXL_BCS_UPCE			                = 102;
     public static final int BXL_BCS_EAN8			                = 103;
     public static final int BXL_BCS_EAN13			                = 104;
     public static final int BXL_BCS_JAN8			                = 105;
     public static final int BXL_BCS_JAN13			                = 106;
     public static final int BXL_BCS_ITF			                = 107;
     public static final int BXL_BCS_CODABAR		                = 108;
     public static final int BXL_BCS_CODE39			                = 109;
     public static final int BXL_BCS_CODE93			                = 110;
     public static final int BXL_BCS_CODE128		                = 111;

     public static final int BXL_BCS_1D_GS1_128                     = 112;
     public static final int BXL_BCS_1D_GS1DATABAR_OMNIDIRECTION    = 113;
     public static final int BXL_BCS_1D_GS1DATABAR_TRUNCATED        = 114;
     public static final int BXL_BCS_1D_GS1DATABAR_LIMITED          = 115;

     //Barcode text position
     public static final int BXL_BC_TEXT_NONE		                = 0;
     public static final int BXL_BC_TEXT_ABOVE	                    = 1;
     public static final int BXL_BC_TEXT_BELOW	                    = 2;
     public static final int BXL_BC_TEXT_BOTH	                    = 3;


}
